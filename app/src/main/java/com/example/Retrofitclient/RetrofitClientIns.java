package com.example.Retrofitclient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientIns {
    private   static Retrofit retrofit;
    private  static final  String BASE_URL = "https://private-anon-746f1cb3f8-githubtrendingapi.apiary-mock.com/";
    public  static  Retrofit getRetrofit()
    {
        if(retrofit==null)
        {
            retrofit=new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }



}
