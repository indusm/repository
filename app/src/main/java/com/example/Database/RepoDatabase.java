package com.example.Database;

import android.content.Context;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.Dao.RepositoryDao;
import com.example.Model.Repos_Pojo;

@Database(entities = {Repos_Pojo.class},version = 3)
public abstract class RepoDatabase extends RoomDatabase {
    private  static  final  String DATABASE_NAME="RepositoryDatabase.db";
    public abstract RepositoryDao repositoryDao();
    private static volatile RepoDatabase INSTANCE;
    public  static RepoDatabase getInstance(Context context)
    {
        if(INSTANCE==null)
        {
            synchronized (RepoDatabase.class){
                if(INSTANCE==null){
                    INSTANCE=Room.databaseBuilder(context,RepoDatabase.class,
                            DATABASE_NAME).addCallback(callback).fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
    static Callback callback=new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new  PopulateAsynTask(INSTANCE);
        }
    };
    static  class  PopulateAsynTask extends AsyncTask<Void,Void,Void>
    {

        private RepositoryDao repositoryDao;

        PopulateAsynTask(RepoDatabase repoDatabase)

        {
            repositoryDao=repoDatabase.repositoryDao();

        }
        @Override
        protected Void doInBackground(Void... voids) {
            repositoryDao.deleteall();
            return null;
        }
    }


}
