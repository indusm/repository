package com.example.task_repo;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.example.APIInterface.Api_Interface;
import com.example.Adapter.Repo_adapter;
import com.example.Helper.ConnectionHelper;
import com.example.Model.Repos_Pojo;
import com.example.Repository.Repo_Repository;
import com.example.Retrofitclient.RetrofitClientIns;
import com.example.ViewModel.RepodbViewModel;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView_repo;
    LinearLayout NoNetwork;
    Toolbar toolbar;
    Button tryagain, offlinerepository;
    SwipeRefreshLayout swipeRefreshLayout;
    private List<Repos_Pojo> reposPojoList_l;
    private Repo_adapter repoAdapter;
    private android.app.AlertDialog.Builder builder;
    private android.app.AlertDialog dialog;
    Context context = MainActivity.this;
    ConnectionHelper helperl;
    private Repo_Repository repo_repository;
    private RepodbViewModel repodbViewModel;
    private  List<Repos_Pojo> repo_listof;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setview();
        setSupportActionBar(toolbar);
       helperl=new ConnectionHelper(context);
//progressbar
        builder=new android.app.AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setView(R.layout.progressbar_layout);
        dialog=builder.create();
//check internetconnection
        if(helperl.isConnectingToInternet())
        {
            dialog.show();
            newtork();

        }
        else
        {
            swipeRefreshLayout.setVisibility(View.GONE);
            NoNetwork.setVisibility(View.VISIBLE);
        }
 //tryagain button to check nd proceed
        tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(helperl.isConnectingToInternet())
                {
                    dialog.show();
                    swipeRefreshLayout.setVisibility(View.VISIBLE);
                    NoNetwork.setVisibility(View.GONE);
                    newtork();

                }
                else
                {
                    swipeRefreshLayout.setVisibility(View.GONE);
                    NoNetwork.setVisibility(View.VISIBLE);
                    Toast.makeText(getApplicationContext(),"Please Connect to internet",Toast.LENGTH_SHORT).show();

                }
            }
        });
        offlinerepository.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                repodbViewModel= ViewModelProviders.of(MainActivity.this).get(RepodbViewModel.class);
                repodbViewModel.getGetallreplist().observe(MainActivity.this, new Observer<List<Repos_Pojo>>() {
                    @Override
                    public void onChanged(List<Repos_Pojo> reposPojoList)
                    {
                        NoNetwork.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        repodbViewModel = ViewModelProviders.of(MainActivity.this).get(RepodbViewModel.class);
                        repodbViewModel.getGetallreplist().observe(MainActivity.this, new Observer<List<Repos_Pojo>>() {
                            @Override
                            public void onChanged(List<Repos_Pojo> reposPojoList) {
                                //Log.v("onchs",""+reposPojoList);
                                repoAdapter=new Repo_adapter(MainActivity.this,reposPojoList);
                                repoAdapter.setrepolist(reposPojoList);
                                recyclerView_repo.setAdapter(repoAdapter);

                            }
                        });

                    }
                });

            }
        });
//swipe to refresh
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                final Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(swipeRefreshLayout.isRefreshing())
                        {
                            swipeRefreshLayout.setRefreshing(false);
                            newtork();
                        }
                    }
                },1500);
            }
        });
        recyclerView_repo.setHasFixedSize(true);
        recyclerView_repo.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView_repo.setItemAnimator(new DefaultItemAnimator());
        repo_listof=new ArrayList<>();
        repo_repository=new Repo_Repository(getApplication());
        repodbViewModel = ViewModelProviders.of(this).get(RepodbViewModel.class);
        repodbViewModel.getGetallreplist().observe(this, new Observer<List<Repos_Pojo>>() {
            @Override
            public void onChanged(List<Repos_Pojo> reposPojoList) {
                //Log.v("onchs",""+reposPojoList);
                repoAdapter=new Repo_adapter(MainActivity.this,reposPojoList);
                repoAdapter.setrepolist(reposPojoList);
                recyclerView_repo.setAdapter(repoAdapter);

            }
        });
    }

    private void newtork() {
        Api_Interface api_interface= RetrofitClientIns.getRetrofit().create(Api_Interface.class);
        Call<List<Repos_Pojo>> call=api_interface.getrepolist();
        call.enqueue(new Callback<List<Repos_Pojo>>() {
            @Override
            public void onResponse(Call<List<Repos_Pojo>> call, Response<List<Repos_Pojo>> response) {
                if(response.isSuccessful())
                {

                    repo_repository.insert(response.body());
                    if(dialog!=null)
                    {
                        dialog.dismiss();
                    }

                }

            }

            @Override
            public void onFailure(Call<List<Repos_Pojo>> call, Throwable t) {
                if(dialog!=null)
                {
                    dialog.dismiss();
                }
                t.printStackTrace();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tryagain.performClick();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem searchiteam=menu.findItem(R.id.search);
        SearchView searchView=(SearchView)menu.findItem(R.id.search).getActionView();
        searchView.setQueryHint("Search Data Here");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                repoAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    private void setview() {
        recyclerView_repo = (RecyclerView) findViewById(R.id.rv_listofrepo);
        NoNetwork=(LinearLayout)findViewById(R.id.nonetwork);
        toolbar=(Toolbar)findViewById(R.id.toolbar);
        swipeRefreshLayout=findViewById(R.id.swipetorefresh);
        offlinerepository=findViewById(R.id.offlinesupport);
        tryagain=findViewById(R.id.tryagain);
        NoNetwork.setVisibility(View.GONE);

    }
}
