package com.example.ViewModel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import com.example.Model.Repos_Pojo;
import com.example.Repository.Repo_Repository;
import java.util.List;

public class RepodbViewModel extends AndroidViewModel {
    private Repo_Repository repo_repository;
    private LiveData<List<Repos_Pojo>> getallreplist;
    public RepodbViewModel(@NonNull Application application) {
        super(application);
        repo_repository = new Repo_Repository(application);
        getallreplist = repo_repository.getGetallrepolist();
    }
    public void insert(List<Repos_Pojo> list) {
        repo_repository.insert(list);
    }

    public LiveData<List<Repos_Pojo>> getGetallreplist() {
        return getallreplist;

    }

}
