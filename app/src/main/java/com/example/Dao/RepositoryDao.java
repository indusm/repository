package com.example.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import com.example.Model.Repos_Pojo;
import java.util.List;

@Dao
public interface RepositoryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<Repos_Pojo> reposPojoList_db);

    @Query("SELECT * FROM Repository_table")
    LiveData<List<Repos_Pojo>> getallrepolistdb();

    @Query("DELETE FROM Repository_table")
    void deleteall();
}
