package com.example.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.Model.Repos_Pojo;
import com.example.task_repo.R;
import java.util.ArrayList;
import java.util.List;

public class Repo_adapter extends RecyclerView.Adapter<Repo_adapter.RepoviewHolder> implements Filterable {
    Context context;
    private List<Repos_Pojo> reposPojoList;
    private List<Repos_Pojo> reposPojoList1;
    private List<Repos_Pojo> reposPojoList_sesrch;
    String Linked="";

    public Repo_adapter(Context context, List<Repos_Pojo> reposPojoList) {
        this.context = context;
        this.reposPojoList = reposPojoList;
    }
    public  void setrepolist(List<Repos_Pojo> reposPojoList)
    {
        this.reposPojoList1=reposPojoList;
        reposPojoList_sesrch=new ArrayList<>(reposPojoList);

    }
    @Override
    public RepoviewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recyclerview_layout, parent, false);
        return new RepoviewHolder(view);
    }

    @Override
    public void onBindViewHolder( RepoviewHolder holder, int position) {
        holder.authorname.setText(this.reposPojoList.get(position).getAuthor());
        holder.sitename.setText(this.reposPojoList.get(position).getName());
        holder.description.setText(this.reposPojoList.get(position).getDescription());
        holder.authorname.setText(this.reposPojoList.get(position).getAuthor());
        holder.link.setText(this.reposPojoList.get(position).getUrl());
         String path=this.reposPojoList.get(position).getAvatar();
         if(path!=null)
         {
             Glide.with(context).load(path).into(holder.authorimage);
         }
         else
         {
             Glide.with(context).load(R.drawable.ic_android_black).into(holder.authorimage);

         }

        Linked=this.reposPojoList.get(position).getUrl();
        holder.language.setText(this.reposPojoList.get(position).getLanguage());
        holder.star.setText(this.reposPojoList.get(position).getStars().toString());
        holder.forks.setText(this.reposPojoList.get(position).getForks().toString());
        holder.link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri=Uri.parse(Linked);
                Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(this.reposPojoList!=null)
        {
            //Log.i("size",""+reposPojoList.size());
            return reposPojoList.size();
        }
       // Log.i("size",""+"000000000");
        return 0;
    }
    @Override
    public Filter getFilter() {
        return searchfilter;
    }
    private Filter searchfilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Repos_Pojo> filteredlist=new ArrayList<>();
            if(charSequence==null || charSequence.length()==0)
            {
                filteredlist.addAll(reposPojoList_sesrch);

            }
            else
            {
                String filterpattern=charSequence.toString().toLowerCase().trim();
                for(Repos_Pojo reposPojo_filter:reposPojoList_sesrch)
                {
                    if (reposPojo_filter.getAuthor().toLowerCase().startsWith(filterpattern))
                    {
                        filteredlist.add(reposPojo_filter);
                    }
                }
            }
            FilterResults results=new FilterResults();
            results.values=filteredlist;
            return results;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            reposPojoList.clear();
            reposPojoList.addAll((List)filterResults.values);
            notifyDataSetChanged();

        }
    };

    public class RepoviewHolder extends RecyclerView.ViewHolder {
        TextView authorname,sitename,description,bulitbyname,link,language,star,forks;
        ImageView authorimage;

        public RepoviewHolder( View itemView) {
            super(itemView);
            authorname=itemView.findViewById(R.id.tv_AuthorName);
            sitename=itemView.findViewById(R.id.tv_nameofsite);
            description=itemView.findViewById(R.id.tv_Description);
            link=itemView.findViewById(R.id.tv_Link);
            language=itemView.findViewById(R.id.tv_language);
            star=itemView.findViewById(R.id.tv_star);
            forks=itemView.findViewById(R.id.tv_forks);
            authorimage=itemView.findViewById(R.id.iv_Authorimage);

        }
    }

}
