package com.example.Repository;

import android.app.Application;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.example.Dao.RepositoryDao;
import com.example.Database.RepoDatabase;
import com.example.Model.Repos_Pojo;
import java.util.List;

public class Repo_Repository {
    private RepoDatabase repoDatabase;
    private LiveData<List<Repos_Pojo>> getallrepolist;
    public Repo_Repository(Application application)
    {

       repoDatabase=RepoDatabase.getInstance(application);
       getallrepolist=repoDatabase.repositoryDao().getallrepolistdb();
    }
    public void insert(List<Repos_Pojo> repolist)
    {

        new InserAsynTask(repoDatabase).execute(repolist);
    }
    public LiveData<List<Repos_Pojo>> getGetallrepolist()
    {
        return  getallrepolist;
    }
    static  class InserAsynTask extends AsyncTask<List<Repos_Pojo>,Void,Void>{
        private RepositoryDao repositoryDao;
        InserAsynTask(RepoDatabase repoDatabase)
        {
            repositoryDao=repoDatabase.repositoryDao();
        }

        @Override
        protected Void doInBackground(List<Repos_Pojo>... lists) {
            repositoryDao.insert(lists[0]);
            return null;
        }
    }
}

