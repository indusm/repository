package com.example.Model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.SerializedName;
@Entity(tableName = "Repository_table")
  public  class Repos_Pojo {

    @PrimaryKey
    @NonNull
    @SerializedName("author")
    @ColumnInfo(name = "Author")
    private String author;

    @SerializedName("name")
    @ColumnInfo(name = "Name")
    private String name;

    @SerializedName("avatar")
    @ColumnInfo(name = "Avatar")
    private String avatar;

    @SerializedName("url")
    @ColumnInfo(name = "Url")
    private String url;

    @SerializedName("description")
    @ColumnInfo(name = "Description")
    private String description;

    @SerializedName("language")
    @ColumnInfo(name = "Language")
    private String language;

    private String languageColor;

    @SerializedName("stars")
    @ColumnInfo(name = "Stars")
    private Long stars;

    @SerializedName("forks")
    @ColumnInfo(name = "Forks")
    private Long forks;

    private Long currentPeriodStars;

    public Repos_Pojo(String author, String name, String avatar, String url, String description, String language, Long stars, Long forks) {
        this.author = author;
        this.name = name;
        this.avatar = avatar;
        this.url = url;
        this.description = description;
        this.language = language;
        this.stars = stars;
        this.forks = forks;

    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLanguageColor() {
        return languageColor;
    }

    public void setLanguageColor(String languageColor) {
        this.languageColor = languageColor;
    }

    public Long getStars() {
        return stars;
    }

    public void setStars(Long stars) {
        this.stars = stars;
    }

    public Long getForks() {
        return forks;
    }

    public void setForks(Long forks) {
        this.forks = forks;
    }

    public Long getCurrentPeriodStars() {
        return currentPeriodStars;
    }

    public void setCurrentPeriodStars(Long currentPeriodStars) {
        this.currentPeriodStars = currentPeriodStars;
    }

    @Override
    public String toString() {
        return "Repos_Pojo{" +
                ", author='" + author + '\'' +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", stars=" + stars +
                ", forks=" + forks +
                '}';
    }

}
