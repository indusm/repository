package com.example.Helper;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ConvertorRepo {
    @TypeConverter
    public ArrayList<String> fromhref(String listofstring)

    {
        Type listtype=new TypeToken<ArrayList<String>>(){}.getType();
        return new Gson().fromJson(listofstring,listtype);
    }
    @TypeConverter
   public String savelist(ArrayList<String> stringList)
    {
        Gson gson=new Gson();
        String json=gson.toJson(stringList);
        return json;
    }
}
