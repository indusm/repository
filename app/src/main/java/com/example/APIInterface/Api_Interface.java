package com.example.APIInterface;

import com.example.Model.Repos_Pojo;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface Api_Interface {
    @GET("repositories?since=daily")
    Call<List<Repos_Pojo>> getrepolist();


}
